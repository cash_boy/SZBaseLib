package com.cn.shuangzi.permission;

/**
 * Created by CN on 2017-10-11.
 */

public abstract class RequestPermissionViewP{
    public abstract void onPermissionRequestSuccess(String[] permissionName);
    public abstract void onRequestPermissionAlertCancelled(String[] permissionName);
    public void onRequestPermissionDeclined(String[] permissionName){
        onRequestPermissionAlertCancelled(permissionName);
    };
}

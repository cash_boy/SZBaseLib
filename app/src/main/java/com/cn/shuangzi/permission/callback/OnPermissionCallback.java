package com.cn.shuangzi.permission.callback;

import androidx.annotation.NonNull;

public interface OnPermissionCallback {

    void onPermissionGranted(@NonNull String[] permissionName);

    void onPermissionDeclined(@NonNull String[] permissionName);

    void onPermissionPreGranted(@NonNull String permissionsName);

    void onPermissionNeedExplanation(@NonNull String permissionName);

    void onPermissionReallyDeclined(@NonNull String[] permissionName);
    /**
     * android6.0以下会触发 因为无论是否判断都返回true
     */
    void onNoPermissionNeeded();
}

package com.cn.shuangzi.activity;

import android.view.View;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;


/**
 * Created by CN on 2018-2-28.
 */

public abstract class SZContactUsActivity extends SZBaseActivity implements View.OnClickListener, SZInterfaceActivity {
    @Override
    protected int onGetChildView() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected void onBindChildViews() {

    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(R.string.txt_contact_us);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtContactUs) {
            if (!SZUtil.joinQQGroup(getActivity(), getQQGroupKey())) {
                SZToast.warning("您可能未安装QQ，或者当前QQ版本不支持！");
            }
        }
    }
    public abstract String getQQGroupKey();
}

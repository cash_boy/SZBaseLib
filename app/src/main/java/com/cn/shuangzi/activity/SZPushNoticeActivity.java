package com.cn.shuangzi.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.bean.PushConfigInfo;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by CN.
 */
public abstract class SZPushNoticeActivity extends SZBaseActivity implements SZInterfaceActivity {
    public LinearLayout lltPushNoticeList;
    private List<PushConfigInfo> pushConfigInfoList;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_push_notice;
    }

    @Override
    protected void onBindChildViews() {
        lltPushNoticeList = findViewById(R.id.lltPushNoticeList);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(R.string.txt_push_notice);
        onReloadData(false);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBarTransparentStyle();
        request(SZRetrofitManager.getInstance().getSZRequest().getPushConfigList(), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                isShowContent(true);
                pushConfigInfoList = new Gson().fromJson(data,new TypeToken<List<PushConfigInfo>>(){}.getType());
                initView();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                showErrorWithMsg(errorCode,errorMsg);
            }
        });
    }
    private void initView(){
        lltPushNoticeList.removeAllViews();
        for (PushConfigInfo pushConfigInfo : pushConfigInfoList){
            lltPushNoticeList.addView(getConfigView(pushConfigInfo));
        }
    }
    private View getConfigView(final PushConfigInfo pushConfigInfo){
        View view = LayoutInflater.from(this).inflate(R.layout.item_push_config_view,null);
        TextView txtPushConfigName = view.findViewById(R.id.txtPushConfigName);
        final Switch switchPushConfigStatus = view.findViewById(R.id.switchPushConfigStatus);
        txtPushConfigName.setText(pushConfigInfo.getAppName());
        switchPushConfigStatus.setChecked(!pushConfigInfo.isClose());
        switchPushConfigStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateConfig(switchPushConfigStatus,pushConfigInfo);
            }
        });
        return view;
    }
    private void updateConfig(final Switch switchPushConfigStatus, final PushConfigInfo pushConfigInfo){
        showBarTransparentStyle();
        request(SZRetrofitManager.getInstance().getSZRequest().updateConfigPush(!switchPushConfigStatus.isChecked(), pushConfigInfo.getApcType())
                , new SZCommonResponseListener() {
                    @Override
                    public void onResponseSuccess(String data) {
                        closeBar();
                        pushConfigInfo.setIsClose(!switchPushConfigStatus.isChecked());
                    }

                    @Override
                    public void onResponseError(int errorCode, String errorMsg) {
                        closeBar();
                        switchPushConfigStatus.setOnCheckedChangeListener(null);
                        switchPushConfigStatus.setChecked(!switchPushConfigStatus.isChecked());
                        switchPushConfigStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                updateConfig(switchPushConfigStatus,pushConfigInfo);
                            }
                        });
                    }
                });
    }
}

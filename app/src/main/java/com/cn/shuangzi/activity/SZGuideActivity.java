package com.cn.shuangzi.activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.fragment.SZGuideFragment;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.util.ArrayList;

import cn.junhua.android.view.IndicatorView;

/**
 * Created by CN.
 */

public abstract class SZGuideActivity extends SZBaseActivity {
    private IndicatorView indicatorView;
    private ViewPager viewPager;
    private TextView txtToMain;
    private ArrayList<Integer> imgBgResList;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_sz_guide;
    }

    @Override
    protected void onBindChildViews() {
        indicatorView = findViewById(R.id.indicatorView);
        viewPager = findViewById(R.id.viewPager);
        txtToMain = findViewById(R.id.txtToMain);
    }

    @Override
    protected void onBindChildListeners() {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                indicatorView.setSelect(position);
                if(position == (imgBgResList.size()-1)){
                    txtToMain.setText("进 入");
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        txtToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getHomeActivity(),null,true);
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        hideNavigationBar();
        imgBgResList = getSerializableExtra();
        if(imgBgResList == null){
            imgBgResList = getBgResList();
        }
        if(!SZValidatorUtil.isValidList(imgBgResList)){
            startActivity(getHomeActivity(),null,true);
            return;
        }
        indicatorView.setCount(imgBgResList.size());
        viewPager.setAdapter(new GuidePagerAdp(getSupportFragmentManager()));
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    class GuidePagerAdp extends FragmentPagerAdapter {

        public GuidePagerAdp(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new SZGuideFragment(imgBgResList.get(position));
        }

        @Override
        public int getCount() {
            return imgBgResList.size();
        }
    }
    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    public abstract ArrayList<Integer> getBgResList();
    public abstract Class getHomeActivity();
}

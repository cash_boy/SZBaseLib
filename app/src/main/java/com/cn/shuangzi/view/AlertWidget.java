package com.cn.shuangzi.view;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.cn.shuangzi.R;


/**********************
 * alertdialog 类封装
 *
 * @author cn
 *
 **********************/

public class AlertWidget {

    protected Context context = null;
    protected AlertDialog.Builder dialog;
    protected Window window;
    protected AlertDialog dlg;

    /********************************
     * 构造函数 实例化一个alert对象
     *
     * @param context
     *            调用该工具类的activity
     ********************************/
    public AlertWidget(Context context) {
        this.context = context;
        this.dialog = new AlertDialog.Builder(this.context);
    }

    public AlertWidget(Context context, boolean isTransparentStyle) {
        this.context = context;
        if (isTransparentStyle)
            this.dialog = new AlertDialog.Builder(this.context, R.style.dialogTranslucent);
        else
            this.dialog = new AlertDialog.Builder(this.context);
    }

    /*******************************
     * 设置alert图标
     *
     * @param icoResourceId
     *            图标的源
     *******************************/
    public void setIcoResourceId(int icoResourceId) {
        if (icoResourceId != 0) {
            this.dialog.setIcon(icoResourceId);
        }
    }

    /*******************************
     * 设置alert标题
     *
     * @param title
     *            标题
     *******************************/
    public void setTitle(String title) {
        dialog.setTitle(title);
    }

    /*******************************
     * 设置alert 显示的信息
     *
     * @param content
     *            内容
     *******************************/
    public void setContent(String content) {
        dialog.setMessage(content);
    }

    /*******************************
     * 设置确定按钮
     *
     * @param btnTXT
     *            按钮上要显示的 内容
     * @param OK
     *            确定按钮触发的事件
     *******************************/
    public void setOKListener(String btnTXT, DialogInterface.OnClickListener OK) {
        dialog.setNeutralButton(btnTXT, OK);
    }

    /*******************************
     * 设置取消按钮
     *
     * @param btnTXT
     *            按钮上要显示的 内容
     * @param cancelListener
     *            确定按钮触发的事件
     *******************************/
    public void setCancelListener(String btnTXT, DialogInterface.OnClickListener cancelListener) {
        dialog.setNegativeButton(btnTXT, cancelListener);
    }

    /*******************************
     * 设置确定按钮
     *
     * @param btnTXT
     *            按钮上要显示的 内容
     * @param OK
     *            确定按钮触发的事件
     *******************************/
    public void setOKListenerWithCancelDefault(String btnTXT, DialogInterface.OnClickListener OK) {
        dialog.setNeutralButton(btnTXT, OK);
        dialog.setNegativeButton(
                context.getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                    }
                });
    }

    /*******************************
     * 设置确定按钮
     *
     * @param OK
     *            确定按钮触发的事件
     *******************************/
    public void setOKListenerWithCancelDefault(DialogInterface.OnClickListener OK) {
        dialog.setNeutralButton(
                context.getString(R.string.txt_sure), OK);
        dialog.setNegativeButton(
                context.getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                    }
                });
    }

    /*******************************
     * 显示alert窗体
     *******************************/
    public void show() {
        if (dlg == null)
            dlg = dialog.create();
        dlg.show();
        window = dlg.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    /**
     * ************************
     * 设置提示框是否可取消
     *
     * @param isCancel ************************
     */
    public void setCancelable(boolean isCancel) {
        if (dialog != null)
            dialog.setCancelable(isCancel);
        if (dlg != null)
            dlg.setCancelable(isCancel);
    }

    /**
     * ************************
     * 设置点击外部提示框是否可取消
     *
     * @param isCancel ************************
     */
    public void setCanceledOnTouchOutside(boolean isCancel) {
        if (dlg != null)
            dlg.setCanceledOnTouchOutside(isCancel);
    }

    /*******************************
     * 设置alert的 子项 仿菜单的alert
     *
     * @param items
     *            子项内容 String[]
     * @param itemsListener
     *            alert中子项单击时触发的事件
     *******************************/
    public void setMenu(String[] items, DialogInterface.OnClickListener itemsListener) {

        dialog.setItems(items, itemsListener);
    }

    /*******************************
     * 设置alert窗体中自定义view
     *
     * @param view
     *******************************/
    public void setView(View view) {
        dialog.setView(view);
    }

    /*******************************
     * 自定义 alertdialog 窗体
     *
     * @param layoutId
     *******************************/
    public void show(int layoutId) {
        if (dlg == null)
            dlg = dialog.create();
        dlg.setCanceledOnTouchOutside(false);
        dlg.show();
        window = dlg.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
        // | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        window.setContentView(layoutId);
    }

    /*******************************
     * 返回自定义 布局对象
     *
     * @return
     *******************************/
    public Window getWindow() {
        return this.window;
    }

    /*******************************
     * 关闭自定义 alertdialog 窗体
     *******************************/
    public void close() {
        if (dlg != null && dlg.isShowing())
            dlg.dismiss();
    }

    /**
     * ************************
     * 获取对话框
     *
     * @return ************************
     */
    public AlertDialog getDlg() {
        return dlg;
    }

    /**
     * ************************
     * 设置撤销监听事件
     *
     * @param listener ************************
     */
    public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
        if (dlg != null)
            dlg.setOnCancelListener(listener);
    }

    public void showCustomEdit(String title, String content, String inputHint, String inputContent, View.OnClickListener onOkClickListener) {
        show(R.layout.alert_edit);
        window.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
        window.findViewById(R.id.btnOk).setOnClickListener(onOkClickListener);
        EditText editText = window.findViewById(R.id.edtTxtContent);
        if (inputHint != null) {
            editText.setHint(inputHint);
        }
        if (inputContent != null) {
            editText.setText(inputContent);
            editText.setSelection(inputContent.length());
        }
        TextView txtTitle = window.findViewById(R.id.txtTitle);
        if (title != null) {
            txtTitle.setText(title);
            txtTitle.setVisibility(View.VISIBLE);
        } else {
            txtTitle.setVisibility(View.GONE);
        }
        TextView txtContent = window.findViewById(R.id.txtContent);
        if (content != null) {
            txtContent.setText(content);
            txtContent.setVisibility(View.VISIBLE);
        } else {
            txtContent.setVisibility(View.GONE);
        }
    }

    /**
     * ************************
     * 对话框是否正在显示
     *
     * @return ************************
     */
    public boolean isDialogShow() {
        if (dlg == null)
            return false;
        return dlg.isShowing();
    }

    public void showBar() {
        close();
        show(R.layout.item_loading);
    }

}

package com.cn.shuangzi.view.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.PopupWindow;

import com.cn.shuangzi.R;
import com.labo.kaji.relativepopupwindow.RelativePopupWindow;

/**
 * Created by CN on 2017-11-21.
 */

public class BasePop extends RelativePopupWindow {
    public Activity baseActivity;
    public WindowManager.LayoutParams wlBackground;
    public OnPopDismissListener onPopDismissListener;
    public View anchorView;
    public boolean hasDarkBg;
    public boolean isBottomMode;
    public boolean isAnimRun;
    public BasePop(final Activity baseActivity, int contentViewResId, final boolean hasDarkBg) {
        super(LayoutInflater.from(baseActivity).inflate(contentViewResId, null), ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true);
        this.baseActivity = baseActivity;
        this.hasDarkBg = hasDarkBg;
//        setOutsideTouchable(false);
        setBackgroundDrawable(new ColorDrawable(baseActivity.getResources().getColor(android.R.color
                .transparent)));
        setAnimationStyle(0);
        setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (hasDarkBg&&wlBackground!=null) {
                    wlBackground.alpha = 1.0f;
                    baseActivity.getWindow().setAttributes(wlBackground);
                }
                if (onPopDismissListener != null)
                    onPopDismissListener.onPopDismissListener();
            }
        });
        //拦截touch事件 自行处理dismiss
        setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int x = (int) event.getX();
                final int y = (int) event.getY();
                if (getContentView() != null && (event.getAction() == MotionEvent.ACTION_DOWN)
                        && ((x < 0) || (x >= getContentView().getWidth()) || (y < 0) || (y >= getContentView().getHeight()))) {
                    dismissPop();
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    dismissPop();
                    return true;
                }
                return false;
            }
        });
    }
    public void showOnAnchorFromBottom(@NonNull View anchor, int vertPos, int horizPos) {
        if(getAnimationStyle() == 0){
            setAnimationStyle(R.style.bottomInPop);
        }
        isBottomMode = true;
        super.showOnAnchor(anchor, vertPos, horizPos);
        anchorView = anchor;
        if (hasDarkBg) {
            // 设置PopupWindow以外部分的背景颜色  有一种变暗的效果
            wlBackground = baseActivity.getWindow().getAttributes();
            wlBackground.alpha = 0.5f;      // 0.0 完全不透明,1.0完全透明
            baseActivity.getWindow().setAttributes(wlBackground);
        }
//        popAnimFromBottom(true);
    }
    public void showOnAnchorFromBottom(@NonNull View anchor) {
        if(getAnimationStyle() == 0){
            setAnimationStyle(R.style.bottomInPop);
        }
        isBottomMode = true;
        super.showOnAnchor(anchor, VerticalPosition.ALIGN_BOTTOM, HorizontalPosition.ALIGN_LEFT);
        anchorView = anchor;
        if (hasDarkBg) {
            // 设置PopupWindow以外部分的背景颜色  有一种变暗的效果
            wlBackground = baseActivity.getWindow().getAttributes();
            wlBackground.alpha = 0.5f;      // 0.0 完全不透明,1.0完全透明
            baseActivity.getWindow().setAttributes(wlBackground);
        }
//        popAnimFromBottom(true);
    }
    public void popAnimFromBottom(final boolean isBottomToTop) {
        if(isAnimRun){
            return;
        }
        isAnimRun = true;
        isBottomMode = true;
        final View contentView = getContentView();
        contentView.setVisibility(View.INVISIBLE);
        contentView.post(new Runnable() {
            @Override
            public void run() {
                Animation animation;
                if (isBottomToTop) {
                    animation = new ScaleAnimation(1f, 1f,
                            0,1, Animation.RELATIVE_TO_SELF,0, Animation.RELATIVE_TO_SELF,1 );
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            contentView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            isAnimRun = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    animation = new ScaleAnimation(1f, 1f,
                            1,0, Animation.RELATIVE_TO_SELF,0, Animation.RELATIVE_TO_SELF,1 );
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            contentView.setVisibility(View.VISIBLE);
                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            dismiss();
                            isAnimRun = false;
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
                animation.setDuration(200);
                contentView.clearAnimation();
                contentView.setAnimation(animation);
                animation.start();
            }
        });
    }
    @Override
    public void showOnAnchor(@NonNull View anchor, int vertPos, int horizPos) {
        super.showOnAnchor(anchor, vertPos, horizPos);
        anchorView = anchor;
        if (hasDarkBg) {
            // 设置PopupWindow以外部分的背景颜色  有一种变暗的效果
            wlBackground = baseActivity.getWindow().getAttributes();
            wlBackground.alpha = 0.5f;      // 0.0 完全不透明,1.0完全透明
            baseActivity.getWindow().setAttributes(wlBackground);
        }
        popAnim(true);
    }

    public void popAnim(final boolean isTopToBottom) {
        if(isAnimRun){
            return;
        }
        isAnimRun = true;
        final View contentView = getContentView();
        contentView.setVisibility(View.INVISIBLE);
        contentView.post(new Runnable() {
            @Override
            public void run() {
                Animation animation;
                if (isTopToBottom) {
                    animation = new ScaleAnimation(1f, 1f,
                            0, 1);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            contentView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            isAnimRun = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                } else {
                    animation = new ScaleAnimation(1f, 1f,
                            1, 0);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            contentView.setVisibility(View.VISIBLE);
                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            dismiss();
                            isAnimRun = false;
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
                animation.setDuration(200);
                contentView.clearAnimation();
                contentView.setAnimation(animation);
                animation.start();
            }
        });
    }

    public void dismissPop() {
        if(isBottomMode){
            if(getAnimationStyle() == 0) {
                popAnimFromBottom(false);
            }else{
                dismiss();
            }
        }else {
            popAnim(false);
        }
    }

    public OnPopDismissListener getOnPopDismissListener() {
        return onPopDismissListener;
    }

    public void setOnPopDismissListener(OnPopDismissListener onPopDismissListener) {
        this.onPopDismissListener = onPopDismissListener;
    }

    public interface OnPopDismissListener {
        void onPopDismissListener();
    }
}

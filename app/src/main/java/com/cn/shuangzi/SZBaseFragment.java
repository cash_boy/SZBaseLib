package com.cn.shuangzi;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.hubert.guide.NewbieGuide;
import com.app.hubert.guide.core.Builder;
import com.app.hubert.guide.model.GuidePage;
import com.app.hubert.guide.model.HighLight;
import com.app.hubert.guide.model.RelativeGuide;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.permission.RequestPermissionHelper;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.retrofit.SZRetrofitResponseListener;
import com.cn.shuangzi.util.SZGlide;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.AlertWidget;
import com.cn.shuangzi.view.pop.BottomCtrlPop;
import com.cn.shuangzi.view.pop.FilterPop;
import com.cn.shuangzi.view.pop.common.CtrlItem;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import io.reactivex.Observable;


/**
 * Fragment基类
 * Created by CN on 2016/10/31.
 */
public abstract class SZBaseFragment extends Fragment {
    private Toolbar toolBar;
    private ImageButton imgLeft;
    private ImageButton imgRight;
    private TextView txtTitle;
    public TextView txtTitleRight;
    public TextView txtTitleLeft;
    private FrameLayout fltContent;
    public View viewChildContent;
    private View includeToolbar;
    public View viewNetError;
    public View viewEmpty;
    public View viewLogin;
    public View rootView;
    private AppCompatActivity activity;
    private AlertWidget bar;
    private String title;
    private View titleCutLine;
    private boolean isPrepared = false;//是否已经准备完成 onChildViewCreated是否执行完毕
    private boolean isFirstLoad = true;//是否第一次网络加载，由子类完成操作
    private View rltBase;
    private BottomCtrlPop bottomCtrlPop;
    private boolean isCanPerformErrorClick;

    public static <T extends SZBaseFragment> T getInstance(Class<T> fragmentClass, Bundle data) {
        try {
            T fragment = fragmentClass.newInstance();
            fragment.setArguments(data);
            return fragment;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T extends SZBaseFragment> T getInstance(Class<T> fragmentClass) {
        return getInstance(fragmentClass, null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (AppCompatActivity) getActivity();
        if (isShowDarkTitleText()) {
            ((SZBaseActivity) getActivity()).setDarkTitleText();
        }
        rootView = LayoutInflater.from(this.getContext()).inflate(R.layout.activity_base, null);
        bindViews();
        if (isShowTitleInit()) {
            toolBar.setVisibility(View.VISIBLE);
        } else {
            toolBar.setVisibility(View.GONE);
        }
        addChildContentView(onGetChildView());
        onBindChildViews();
        onBindChildListeners();
        fltContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (fltContent.getWidth() > 0) {
                    fltContent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    onChildViewCreated();
                    isPrepared = true;
                }
            }
        });
        return rootView;
    }

    private void bindViews() {
        includeToolbar = rootView.findViewById(R.id.includeToolbar);
        toolBar = (Toolbar) includeToolbar.findViewById(R.id.toolBar);
        titleCutLine = includeToolbar.findViewById(R.id.titleCutLine);
        imgLeft = (ImageButton) includeToolbar.findViewById(R.id.imgLeft);
        imgRight = (ImageButton) includeToolbar.findViewById(R.id.imgRight);
        txtTitle = (TextView) includeToolbar.findViewById(R.id.txtTitle);
        txtTitleRight = (TextView) includeToolbar.findViewById(R.id.txtTitleRight);
        txtTitleLeft = (TextView) includeToolbar.findViewById(R.id.txtTitleLeft);
        fltContent = (FrameLayout) rootView.findViewById(R.id.fltContent);
        rltBase = rootView.findViewById(R.id.rltBase);
        viewNetError = rootView.findViewById(R.id.viewNetError);
        isCanPerformErrorClick = true;
        viewNetError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCanPerformErrorClick) {
                    isShowError(false);
                    onReloadData(false);
                }
            }
        });
        viewEmpty = rootView.findViewById(R.id.viewEmpty);
        viewLogin = findViewById(R.id.viewLogin);
        viewLogin.findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginBtnClick();
            }
        });
    }

    /**
     * 添加子contentView
     *
     * @param resId
     */
    private View addChildContentView(int resId) {
        if (resId != 0) {
            viewChildContent = LayoutInflater.from(this.getContext()).inflate(resId, null);
            fltContent.addView(viewChildContent);
            return viewChildContent;
        } else {
            return fltContent;
        }

    }

    public void showBackImgLeft(int backResId) {
        setImgLeftBg(backResId);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackBtnPressed();
                getActivity().finish();
            }
        });
    }

    public void onBackBtnPressed() {
    }

    /**
     * 获取子view
     *
     * @return
     */
    protected View getChildView() {
        return viewChildContent;
    }

    /**
     * 获取子view的layout
     *
     * @return
     */
    protected abstract int onGetChildView();

    /**
     * 初始化绑定view控件
     */
    protected abstract void onBindChildViews();

    /**
     * 初始化绑定监听事件
     */
    protected abstract void onBindChildListeners();

    /**
     * 子view创建完成
     */
    protected abstract void onChildViewCreated();

    /**
     * ******************
     * 点击error界面重新加载数据
     * ******************
     */
    protected abstract void onReloadData(boolean isRefresh);

    /**
     * ******************
     * 点击登录按钮
     * ******************
     */
    public void onLoginBtnClick() {
    }


    /**
     * 判断是否登录
     *
     * @param isOpenLogin 是否需要打开登录页
     * @return
     */
    public boolean isLogin(boolean isOpenLogin) {
        return false;
    }

    /**
     * *************************
     * 所有继承BaseFragment的子类都将
     * 在这个方法中实现物理Back键按下后的逻辑，
     * FragmentActivity捕捉到物理返回键点击事件后
     * 会首先询问Fragment是否消费该事件，
     * 如果没有Fragment消息时FragmentActivity自己才会消费该事件
     * *************************
     */
    public boolean onBackPressed() {
        return false;
    }

    public <T extends View> T findViewById(int id) {
//        return getChildView().findViewById(id);
        return rootView.findViewById(id);
    }

    public void isShowTitleCutLine(boolean isShow) {
        titleCutLine.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 设置标题
     *
     * @param resId
     */
    public void setTitleTxt(int resId) {
        setTitleTxt(getString(resId));
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitleTxt(String title) {
        this.title = title;
        txtTitle.setText(title);
    }

    public TextView getTxtTitle() {
        return this.txtTitle;
    }

    public String getTitle() {
        return this.title;
    }

    public void isShowTitle(final boolean isShow) {
        includeToolbar.post(new Runnable() {
            @Override
            public void run() {
                if (isShow) {
                    includeToolbar.setVisibility(View.VISIBLE);
                } else {
                    includeToolbar.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 设置标题背景颜色
     *
     * @param colorResId
     */
    public void setToolBarColor(int colorResId) {
        toolBar.setBackgroundColor(getResources().getColor(colorResId));
    }

    /**
     * 获取标题左侧imgBtn
     *
     * @return
     */
    public ImageButton getImgLeft() {
        return this.imgLeft;
    }

    public void isShowImgLeft(boolean isShow) {
        if (isShow) {
            imgLeft.setVisibility(View.VISIBLE);
        } else {
            imgLeft.setVisibility(View.GONE);
        }
    }

    public void setImgLeftBg(int resId) {
        this.imgLeft.setImageResource(resId);
        isShowImgLeft(true);
    }

    public void setImgLeftBg(int resId, View.OnClickListener onClickListener) {
        this.imgLeft.setImageResource(resId);
        this.imgLeft.setOnClickListener(onClickListener);
        isShowImgLeft(true);
    }


    /**
     * 获取右侧imgBtn
     *
     * @return
     */
    public ImageButton getImgRight() {
        return this.imgRight;
    }

    public void isShowImgRight(boolean isShow) {
        if (isShow) {
            imgRight.setVisibility(View.VISIBLE);
        } else {
            imgRight.setVisibility(View.GONE);
        }
    }

    public void setImgRightBg(int resId) {
        this.imgRight.setImageResource(resId);
        isShowImgRight(true);
    }

    public void setImgRightBg(int resId, View.OnClickListener onClickListener) {
        this.imgRight.setImageResource(resId);
        this.imgRight.setOnClickListener(onClickListener);
        isShowImgRight(true);
    }

    public void setTxtTitleRight(int resId) {
        setTxtTitleRight(getString(resId));
    }

    public void setTxtTitleRight(int resId, View.OnClickListener onClickListener) {
        setTxtTitleRight(getString(resId));
        txtTitleRight.setOnClickListener(onClickListener);
    }

    public void setTxtTitleRight(String titleRight) {
        txtTitleRight.setText(titleRight);
        isShowTxtRight(true);
    }

    public void setTxtTitleRight(String titleRight, View.OnClickListener onClickListener) {
        txtTitleRight.setText(titleRight);
        txtTitleRight.setOnClickListener(onClickListener);
        isShowTxtRight(true);
    }

    public void isShowTxtRight(boolean isShow) {
        if (isShow) {
            txtTitleRight.setVisibility(View.VISIBLE);
        } else {
            txtTitleRight.setVisibility(View.GONE);
        }
    }

    public TextView getTxtTitleRight() {
        return txtTitleRight;
    }

    public void setTxtTitleLeft(int resId) {
        setTxtTitleLeft(getString(resId));
    }

    public void setTxtTitleLeft(int resId, View.OnClickListener onClickListener) {
        setTxtTitleLeft(getString(resId));
        txtTitleLeft.setOnClickListener(onClickListener);
    }

    public void setTxtTitleLeft(String titleLeft) {
        txtTitleLeft.setText(titleLeft);
        isShowTxtLeft(true);
    }

    public void setTxtTitleLeft(String titleLeft, View.OnClickListener onClickListener) {
        txtTitleLeft.setText(titleLeft);
        txtTitleLeft.setOnClickListener(onClickListener);
        isShowTxtLeft(true);
    }

    public TextView getTxtTitleLeft() {
        return txtTitleLeft;
    }

    public void isShowTxtLeft(boolean isShow) {
        if (isShow) {
            txtTitleLeft.setVisibility(View.VISIBLE);
        } else {
            txtTitleLeft.setVisibility(View.GONE);
        }
    }

    /**
     * 获取toolBar
     *
     * @return
     */
    public Toolbar getToolBar() {
        return this.toolBar;
    }

    /**
     * 获取状态栏的高度
     *
     * @return
     */
    public int getStatusBarHeight() {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            return getResources().getDimensionPixelSize(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SZUtil.dip2px(24);
    }

    public void setWhiteStatusBarDarkText() {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            setStatusBarColor(android.R.color.white);
            setStatusBarTextColorStyleBelowBar(true);
        } else {
            setStatusBarColor(R.color.colorStatusBar);
        }
    }

    public void setStatusBarTextColorStyle(boolean dark) {
        View decor = activity.getWindow().getDecorView();
        if (dark) {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

    public void setStatusBarTextColorStyleBelowBar(boolean dark) {
        View decor = activity.getWindow().getDecorView();
        if (dark) {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

    public void setDarkStatusText() {
        setStatusBarTextColorStyle(true);
    }

    public void setWhiteStatusText() {
        setStatusBarTextColorStyle(false);
    }

    public void setTransparentStatusBarWhiteText() {
        setStatusBarTrans();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            setStatusBarTextColorStyle(false);
        }
    }

    /**
     * 状态栏透明
     */
    public void setStatusBarTrans() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//21表示5.0
            Window window = activity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//19表示4.4
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //虚拟键盘也透明
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    public void setStatusBarColor(int colorResId) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                activity.getWindow().setStatusBarColor(getResources().getColor(colorResId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStatusBarColor(String color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                activity.getWindow().setStatusBarColor(Color.parseColor(color));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 启动activity
     *
     * @param tClass
     * @param isClose 是否关闭本界面
     */
    public void startActivity(Class<?> tClass, Bundle extras, boolean isClose) {
        Intent intent = new Intent(activity, tClass);
        if (extras != null)
            intent.putExtras(extras);
        startActivity(intent);
        if (isClose)
            activity.finish();
    }

    /**
     * 启动activity
     *
     * @param tClass
     * @param isClose 是否关闭本界面
     */
    public void startActivity(Class<?> tClass, boolean isClose) {
        startActivity(tClass, null, isClose);
    }

    /**
     * 启动activity
     *
     * @param tClass
     */
    public void startActivity(Class<?> tClass) {
        startActivity(tClass, null, false);
    }

    public void startActivity(Serializable data, Class to) {
        Intent intent = new Intent(getActivity(), to);
        intent.putExtra(to.getSimpleName(), data);
        startActivity(intent);
    }

    public void startActivity(String data, Class to) {
        Intent intent = new Intent(getActivity(), to);
        intent.putExtra(to.getSimpleName(), data);
        startActivity(intent);
    }

    /**
     * 获取网络请求的tag
     *
     * @return
     */
    public String getRequestTag() {
        return this.getClass().getSimpleName();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isPrepared) {
            super.setUserVisibleHint(isVisibleToUser);
            if (getUserVisibleHint()) {
                onVisible();
            } else {
                onInvisible();
            }
        }
    }

    /**
     * *******************
     * 当前fragment为可见状态，
     * 可在可见时，再去加载网络数据
     * *******************
     */
    public void onVisible() {
        SZManager.getInstance().onUMPageStart(getRequestTag()); //统计页面("MainScreen"为页面名称，可自定义)
    }

    /**
     * *******************
     * 当前fragment为不可见状态
     * *******************
     */
    public void onInvisible() {
        SZManager.getInstance().onUMPageEnd(getRequestTag()); //统计页面("MainScreen"为页面名称，可自定义)
    }

    /**
     * 通过tag撤销请求
     */
    public void cancelByTag(String tag) {
        if (tag == null)
            return;
        SZRetrofitManager.getInstance().cancelDisposable(tag);
    }

    public void isShowContent(boolean isShow) {
        if (bar != null)
            bar.close();
        if (isShow) {
            fltContent.setVisibility(View.VISIBLE);
            isShowError(false);
            isShowEmpty(false);
            isShowLogin(false);
        } else {
            fltContent.setVisibility(View.GONE);
        }
    }

    public void showError(int imgErrorRes, int txtErrorRes) {
        showError(imgErrorRes, getString(txtErrorRes));
    }

    public void showError(int imgErrorRes, String txtError) {
        closeBar();
        isCanPerformErrorClick = false;
        viewNetError.setVisibility(View.VISIBLE);
        isShowContent(false);
        isShowEmpty(false);
        isShowLogin(false);
        try {
            if (imgErrorRes != 0) {
                ((ImageView) viewNetError.findViewById(R.id.imgNetError)).setImageResource(imgErrorRes);
            }else{
                ((ImageView) viewNetError.findViewById(R.id.imgNetError)).setImageResource(R.mipmap.ic_data_error);
            }
        } catch (Exception e) {
        }
        try {
            if (txtError != null) {
                ((TextView) viewNetError.findViewById(R.id.txtHintTop)).setText(txtError);
            }
        } catch (Exception e) {
        }
        try {
            viewNetError.findViewById(R.id.txtHintBottom).setVisibility(View.GONE);
        } catch (Exception e) {
        }
    }
    public void showErrorWithMsg(int errorResId,int errorId,String errorMsg) {
        if(errorId == SZRetrofitManager.ERROR_NET_FAILED){
            isShowError(true);
        }else{
            if(errorId == SZRetrofitManager.ERROR_EXCEPTION || TextUtils.isEmpty(errorMsg)) {
                showError(errorResId, R.string.txt_service_error);
            }else{
                showError(errorResId,errorMsg);
            }
        }
    }

    public void showErrorWithMsg(int errorId,String errorMsg) {
        showErrorWithMsg(SZApp.getInstance().getServiceErrorImgResId(),errorId,errorMsg);
    }

    public void isShowError(boolean isShow) {
        if (bar != null)
            bar.close();
        if (isShow) {
            isCanPerformErrorClick = true;
            try {
                ((TextView) viewNetError.findViewById(R.id.txtHintTop)).setText(getString(R.string.txt_net_error_hint_top));
            } catch (Exception e) {
            }
            try {
                ((TextView) viewNetError.findViewById(R.id.txtHintBottom)).setText(getString(R.string.txt_net_error_hint_bottom));
                viewNetError.findViewById(R.id.txtHintBottom).setVisibility(View.VISIBLE);
            } catch (Exception e) {
            }

            ((ImageView) viewNetError.findViewById(R.id.imgNetError)).setImageResource(R.mipmap.ic_error);

            viewNetError.setVisibility(View.VISIBLE);
            isShowContent(false);
            isShowEmpty(false);
            isShowLogin(false);
        } else {
            viewNetError.setVisibility(View.GONE);
        }
    }

    public void isShowEmpty(boolean isShow) {
        if (bar != null)
            bar.close();
        if (isShow) {
            viewEmpty.setVisibility(View.VISIBLE);
            isShowContent(false);
            isShowError(false);
            isShowLogin(false);
        } else {
            viewEmpty.setVisibility(View.GONE);
        }
    }

    public void isShowLogin(boolean isShow) {
        if (bar != null)
            bar.close();
        if (isShow) {
            viewLogin.setVisibility(View.VISIBLE);
            isShowContent(false);
            isShowError(false);
            isShowEmpty(false);
        } else {
            viewLogin.setVisibility(View.GONE);
        }
    }

    public void setEmptyImg(int resId) {
        ((ImageView) viewEmpty.findViewById(R.id.imgEmpty)).setImageResource(resId);
    }

    public void setEmptyTxt(int resId) {
        ((TextView) viewEmpty.findViewById(R.id.txtEmpty)).setText(resId);
    }

    public void showBar() {
        showBar(false, false);
    }

    public void showBarTransparentStyle() {
        showBar(false, true);
    }

    public void showBar(boolean cancelable) {
        showBar(cancelable, false);
    }

    public void showBar(boolean cancelable, boolean isTransparentStyle) {
        if (bar == null)
            bar = new AlertWidget(getActivity(), isTransparentStyle);
        bar.setCancelable(cancelable);
        bar.setCanceledOnTouchOutside(cancelable);
        if (!bar.isDialogShow())
            bar.showBar();
    }

    public void closeBar() {
        if (bar != null)
            bar.close();
    }

    public void setPaddingStatusBarTrans() {
        rltBase.setPadding(0, getStatusBarHeight(), 0, 0);
    }

    public boolean isPrepared() {
        return isPrepared;
    }

    public boolean isFirstLoad() {
        return isFirstLoad;
    }

    public void setFirstLoad(boolean firstLoad) {
        isFirstLoad = firstLoad;
    }

    public View getBaseView() {
        return this.rltBase;
    }


    public void showBottomCtrl(List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener) {
        showBottomCtrl(ctrlItemList, onItemClickListener, null, false);
    }

    public void showBottomCtrl(List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener, String title, boolean isReset) {
        if (bottomCtrlPop == null || isReset) {
            bottomCtrlPop = new BottomCtrlPop(getActivity(), ctrlItemList, onItemClickListener, title);
        }
        bottomCtrlPop.showOnAnchorFromBottom(((SZBaseActivity) getActivity()).getBaseView());
    }
    public void showBottomCtrl(List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener, String title, boolean isReset,boolean isCheckMode) {
        if (bottomCtrlPop == null || isReset) {
            bottomCtrlPop = new BottomCtrlPop(getActivity(), ctrlItemList, onItemClickListener,isCheckMode, title);
        }
        bottomCtrlPop.showOnAnchorFromBottom(((SZBaseActivity) getActivity()).getBaseView());
    }

    public SweetAlertDialog showBuyVipAlert(final Class buyVipClass){
        return showWarningAlert("您还不是尊贵VIP用户", "是否前去购买？", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                startActivity(buyVipClass);
            }
        });
    }
    public SweetAlertDialog showSuccessAlert(String title, String content) {
        return showAlert(SweetAlertDialog.SUCCESS_TYPE, title, content, null, null, null, null, false);
    }
    public SweetAlertDialog showSuccessAlert(String title, String content,String okText, SweetAlertDialog.OnSweetClickListener onClickListener) {
        return showAlert(SweetAlertDialog.SUCCESS_TYPE, title, content, okText, onClickListener, null, null, false);
    }

    public SweetAlertDialog showAlert(int type,String title, String content,String okText, SweetAlertDialog.OnSweetClickListener onClickListener) {
        return showAlert(type, title, content, okText, onClickListener, null, null, false);
    }
    public SweetAlertDialog showWarningAlert(String title, String content, SweetAlertDialog.OnSweetClickListener onOkClickListener) {
        return showWarningAlert(title, content, null, null, onOkClickListener);
    }

    public SweetAlertDialog showWarningAlert(String title, String content, String okText, String cancelText, SweetAlertDialog.OnSweetClickListener onOkClickListener) {
        return showAlert(SweetAlertDialog.WARNING_TYPE, title, content, okText, onOkClickListener, cancelText, null, true);
    }

    public SweetAlertDialog showErrorAlert(String title, String content, String okText) {
        return showAlert(SweetAlertDialog.ERROR_TYPE, title, content, okText, null, null, null, false);
    }

    public SweetAlertDialog showNormalAlert(String title, String content, String okText) {
        return showAlert(SweetAlertDialog.NORMAL_TYPE, title, content, okText, null, null, null, false);
    }

    public SweetAlertDialog showAlert(int type, String title, String content, String okText, SweetAlertDialog.OnSweetClickListener onOkClickListener,
                                      String cancelText, SweetAlertDialog.OnSweetClickListener onCancelClickListener, boolean isShowCancelButton) {
        return ((SZBaseActivity) getActivity()).showAlert(type, title, content, okText, onOkClickListener, cancelText, onCancelClickListener, isShowCancelButton);
    }

    public SweetAlertDialog showAlert(int type, String title, String content, String okText, SweetAlertDialog.OnSweetClickListener onOkClickListener,
                                      String cancelText, SweetAlertDialog.OnSweetClickListener onCancelClickListener, boolean isShowCancelButton, boolean isShowCloseButton
            , boolean isCanceledOnTouchOutside) {
        return ((SZBaseActivity) getActivity()).
                showAlert(type, title, content, okText, onOkClickListener,
                        cancelText, onCancelClickListener, isShowCancelButton, isShowCloseButton
                        , isCanceledOnTouchOutside);
    }
    public void showGuide(int layout, View highLightView, HighLight.Shape shape, String tag, boolean isShowAlways){
        ((SZBaseActivity)getActivity()).showGuide(layout,highLightView,shape,tag,isShowAlways);
    }
    public void showGuideList(List<Integer> layoutList,List<View> highLightViewList,List<HighLight.Shape> shapeList, String tag,boolean isShowAlways){
        ((SZBaseActivity)getActivity()).showGuideList(layoutList,highLightViewList,shapeList,tag,isShowAlways);
    }
    public void showRelativeGuide(RelativeGuide relativeGuide, View highLightView, HighLight.Shape shape, String tag, boolean isShowAlways){
        ((SZBaseActivity)getActivity()).showRelativeGuide(relativeGuide,highLightView,shape,tag,isShowAlways);
    }
    public void showRelativeGuideList(List<RelativeGuide> relativeGuideList,List<View> highLightViewList,List<HighLight.Shape> shapeList, String tag,boolean isShowAlways){
        ((SZBaseActivity)getActivity()).showRelativeGuideList(relativeGuideList,highLightViewList,shapeList,tag,isShowAlways);
    }
    public void setRequestPermissionHelper(RequestPermissionHelper requestPermissionHelper) {
        ((SZBaseActivity) getActivity()).setRequestPermissionHelper(requestPermissionHelper);
    }

    public void requestPermission(RequestPermissionHelper requestPermissionHelper) {
        ((SZBaseActivity) getActivity()).requestPermission(requestPermissionHelper);
    }

    public void request(Observable observable, SZRetrofitResponseListener retrofitResponseListener) {
        SZRetrofitManager.getInstance().request(observable, getRequestTag(), retrofitResponseListener);
    }

    public void loadImg(ImageView imageView, String url) {
        if (imageView != null) {
            SZGlide.load(this, url,imageView);
        }
    }

    public void loadImg(ImageView imageView, String url,int placeholder) {
        if (imageView != null) {
            SZGlide.load(this, url,imageView,placeholder);
        }
    }
    public void onResume() {
        super.onResume();
        SZManager.getInstance().onUMPageStart(getRequestTag()); //统计页面("MainScreen"为页面名称，可自定义)
    }

    public void onPause() {
        super.onPause();
        SZManager.getInstance().onUMPageEnd(getRequestTag());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cancelByTag(getRequestTag());
    }

    private boolean isRegist;

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelByTag(getRequestTag());
        if (isRegist) {
            try {
                EventBus.getDefault().unregister(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void registEventBus() {
        isRegist = true;
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isDealOnReloadData(List list){
        if(SZValidatorUtil.isValidList(list)){
            if(list.size()<= SZConst.LIMIT_MAX) {
                return false;
            }
        }
        onReloadData(false);
        return true;
    }

    public boolean isShowTitleInit() {
        return true;
    }

    public boolean isShowDarkTitleText() {
        return false;
    }

    private boolean isNeedLogin = false;

    public boolean isNeedLogin() {
        return isNeedLogin;
    }

    public void setNeedLogin(boolean needLogin) {
        isNeedLogin = needLogin;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
    }


}

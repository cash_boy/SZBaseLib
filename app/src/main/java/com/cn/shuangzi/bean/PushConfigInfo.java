package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class PushConfigInfo implements Serializable {
    private String appName;
    private String apcType;
    private boolean apcIsClose;

    public boolean isClose() {
        return apcIsClose;
    }

    public void setIsClose(boolean apcIsClose) {
        this.apcIsClose = apcIsClose;
    }

    public String getAppName() {
        return appName;
    }

    public String getApcType() {
        return apcType;
    }

    @Override
    public String toString() {
        return "PushConfigInfo{" +
                "appName='" + appName + '\'' +
                ", apcType='" + apcType + '\'' +
                ", apcIsClose=" + apcIsClose +
                '}';
    }
}

package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class VipInfo implements Serializable{
    private String memberConsumerId;
    private String memberId;
    private String memberType;
    private long memberUpdateTime;
    private long memberCreateTime;
    private long memberValidityTime;
    private boolean isCheck;

    public String getMemberConsumerId() {
        return memberConsumerId;
    }

    public String getMemberId() {
        return memberId;
    }

    public String getMemberType() {
        return memberType;
    }

    public long getMemberUpdateTime() {
        return memberUpdateTime;
    }

    public long getMemberCreateTime() {
        return memberCreateTime;
    }

    public long getMemberValidityTime() {
        return memberValidityTime;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public VipInfo setCheck(boolean check) {
        isCheck = check;
        return this;
    }

    public VipInfo setMemberValidityTime(long memberValidityTime) {
        this.memberValidityTime = memberValidityTime;
        return this;
    }

    public VipInfo setMemberId(String memberId) {
        this.memberId = memberId;
        return this;
    }

    @Override
    public String toString() {
        return "VipInfo{" +
                "memberConsumerId='" + memberConsumerId + '\'' +
                ", memberId='" + memberId + '\'' +
                ", memberType='" + memberType + '\'' +
                ", memberUpdateTime=" + memberUpdateTime +
                ", memberCreateTime=" + memberCreateTime +
                ", memberValidityTime=" + memberValidityTime +
                '}';
    }
}

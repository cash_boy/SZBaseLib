package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class MiniProgramInfo implements Serializable {
    private String originalId;
    private String path;

    public String getOriginalId() {
        return originalId;
    }

    public void setOriginalId(String originalId) {
        this.originalId = originalId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "MiniProgramInfo{" +
                "originalId='" + originalId + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}

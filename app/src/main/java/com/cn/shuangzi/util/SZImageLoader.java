package com.cn.shuangzi.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.cn.shuangzi.GlideApp;
import com.cn.shuangzi.GlideOptions;
import com.cn.shuangzi.GlideRequest;
import com.cn.shuangzi.GlideRequests;

import java.io.File;

/**
 * 图片加载器
 * Created by CN on 2018/1/10.
 */
public class SZImageLoader {
    private Activity activity;
    private FragmentActivity fragmentActivity;
    private Fragment fragment;
    private androidx.fragment.app.Fragment fragmentX;
    private Context context;

    public SZImageLoader(Activity activity) {
        this.activity = activity;
    }

    public SZImageLoader(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
    }

    public SZImageLoader(Fragment fragment) {
        this.fragment = fragment;
    }

    public SZImageLoader(androidx.fragment.app.Fragment fragmentX) {
        this.fragmentX = fragmentX;
    }

    public SZImageLoader(Context context) {
        this.context = context;
    }


    public void load(int imgViewId, String url) {
        load(getImageView(imgViewId), url, null, 0);
    }

    public void load(int imgViewId, String url, int placeholderResId) {
        load(getImageView(imgViewId), url, null, placeholderResId);
    }

    public void load(int imgViewId, String url, ImageView.ScaleType scaleType, int placeholderResId) {
        load(getImageView(imgViewId), url, scaleType, placeholderResId, null);
    }

    public void load(ImageView imgView, String url) {
        load(imgView, url, null, 0);
    }

    public void load(ImageView imgView, String url, int placeholderResId) {
        load(imgView, url, null, placeholderResId);
    }

    public void load(ImageView imgView, String url, ImageView.ScaleType scaleType, int placeholderResId) {
        load(imgView, url, scaleType, placeholderResId, null);
    }

    public void load(ImageView imgView, String url, ImageView.ScaleType scaleType, int placeholderResId, RequestListener listener) {
        load(imgView, url, scaleType, placeholderResId, listener, null,true);
    }

    public void load(ImageView imgView, String url, ImageView.ScaleType scaleType, int placeholder, RequestListener listener, String signature,boolean isOriginalSize) {
        if (imgView == null) {
            return;
        }
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            GlideRequest glideRequest = glideRequests.load(url);//.transition(DrawableTransitionOptions.withCrossFade());
            if (signature != null) {
                glideRequest = glideRequest.signature(new ObjectKey(signature));
            }
            if (listener != null) {
                glideRequest = glideRequest.listener(listener);
            }
            if (scaleType != null) {
                imgView.setScaleType(scaleType);
            } else {
                if(imgView.getScaleType() == null) {
                    imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            }
            if (placeholder != 0) {
                glideRequest.placeholder(placeholder);
            }
            if(isOriginalSize) {
                glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            }
            glideRequest.into(imgView);
        }
    }

    public void loadTarget(String url, Target target) {
        if (url == null||target == null) {
            return;
        }
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            GlideRequest glideRequest = glideRequests.load(url);
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            glideRequest.into(target);
        }
    }

    public void loadTarget(String url,SimpleTarget<Bitmap> simpleTarget) {
        if (url == null) {
            return;
        }
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            glideRequests.asBitmap().load(url).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).into(simpleTarget);
        }
    }
    public void load(ImageView imgView, int loadResId) {
        if (imgView == null) {
            return;
        }
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            glideRequests.load(loadResId).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).into(imgView);
        }
    }

    public void load(ImageView imgView, File file,ImageView.ScaleType scaleType, int placeholder) {
        if (imgView == null) {
            return;
        }
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            GlideOptions glideOptions = new GlideOptions();
            glideOptions.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE);
            glideRequests.setDefaultRequestOptions(glideOptions);
            GlideRequest glideRequest = glideRequests.load(file).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            if (scaleType != null) {
                imgView.setScaleType(scaleType);
            } else {
                if(imgView.getScaleType() == null) {
                    imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            }
            if (placeholder != 0) {
                glideRequest = glideRequest.placeholder(placeholder);
            }
            glideRequest.into(imgView);
        }
    }
    public ImageView getImageView(int id) {
        if (activity != null) {
            return activity.findViewById(id);
        } else if (fragmentActivity != null) {
            return fragmentActivity.findViewById(id);
        } else if (fragment != null) {
            return fragment.getActivity().findViewById(id);
        } else if (fragmentX != null) {
            return fragmentX.getActivity().findViewById(id);
        } else if (context != null) {
            return ((Activity)context).findViewById(id);
        }
        return null;
    }
    public GlideRequests getGlideRequests() {
        if (activity != null) {
            return GlideApp.with(activity);
        } else if (fragmentActivity != null) {
            return GlideApp.with(fragmentActivity);
        } else if (fragment != null) {
            return GlideApp.with(fragment);
        } else if (fragmentX != null) {
            return GlideApp.with(fragmentX);
        } else if (context != null) {
            return GlideApp.with(context);
        }
        return null;
    }

}

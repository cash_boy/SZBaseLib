package com.cn.shuangzi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by CN.
 */
public class ZoneUtil {

    public static SimpleDateFormat sfRecordShow = new SimpleDateFormat("MM月dd日 HH:mm");
    public static SimpleDateFormat sfRecordAllData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat sfRecordAllShow = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
    public static SimpleDateFormat sfRecordSimpleShow = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat sfRecordData = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat sfStatisticShow = new SimpleDateFormat("MM-dd");
    public static SimpleDateFormat sfMonthDateShow = new SimpleDateFormat("MM月dd日");
    public static SimpleDateFormat sfMonthDateHourMinuteShow = new SimpleDateFormat("MM月dd日 HH:mm");

    static {
        sfRecordSimpleShow.setTimeZone(TimeZone.getDefault());
        sfRecordAllShow.setTimeZone(TimeZone.getDefault());
        sfRecordShow.setTimeZone(TimeZone.getDefault());
        sfMonthDateHourMinuteShow.setTimeZone(TimeZone.getDefault());
        sfMonthDateShow.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        sfStatisticShow.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        sfRecordAllData.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        sfRecordData.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
    }

    public static String getShowDateAllShow(Date date){
        return sfRecordAllShow.format(date);
    }
    public static String getDataDateAll(Date date){
        return sfRecordAllData.format(date);
    }
    public static String getTimeSimpleShow(Date date) {
        return sfRecordSimpleShow.format(date);
    }

    public static String getMonthDayDateShow(Date date) {
        return sfMonthDateShow.format(date);
    }

    public static String getMonthDayHourMinuteDate(Date date) {
        return sfMonthDateHourMinuteShow.format(date);
    }
    public static Date getDateCurrentZone() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        return calendar.getTime();
    }

    public static String getCurrentTimeZone() {
        TimeZone tz = TimeZone.getDefault();
        return createGmtOffsetString(false, true, tz.getRawOffset());
    }

    public static String createGmtOffsetString(boolean includeGmt,
                                               boolean includeMinuteSeparator, int offsetMillis) {
        int offsetMinutes = offsetMillis / 60000;
        char sign = '+';
        if (offsetMinutes < 0) {
            sign = '-';
            offsetMinutes = -offsetMinutes;
        }
        StringBuilder builder = new StringBuilder(9);
        if (includeGmt) {
            builder.append("GMT");
        }
        builder.append(sign);
        appendNumber(builder, 2, offsetMinutes / 60);
        if (includeMinuteSeparator) {
            builder.append(':');
        }
        appendNumber(builder, 2, offsetMinutes % 60);
        return builder.toString();
    }

    public static void appendNumber(StringBuilder builder, int count, int value) {
        String string = Integer.toString(value);
        for (int i = 0; i < count - string.length(); i++) {
            builder.append('0');
        }
        builder.append(string);
    }
    public static String getDistanceBeforeNow(long timeDate) {
        if (timeDate <= 0)
            return "";
        StringBuffer sb = new StringBuffer();
        long time = System.currentTimeMillis() - timeDate;
        long mill = (long) Math.ceil(time / 1000);//秒前

        long minute = (long) Math.ceil(time / 60 / 1000.0f);// 分钟钟前

        long hour = (long) Math.ceil(time / 60 / 60 / 1000.0f);// 小时

        long day = (long) Math.ceil(time / 24 / 60 / 60 / 1000.0f);// 天前
        if (time < 0) {
            return "";
        }
        if (day - 1 > 0) {
            return "";
        }
        if (hour - 1 > 0) {
            if (hour > 24) {
                return "";
            } else {
                long hourTemp = minute / 60;
                if (hourTemp >= 1) {
                    minute -= hourTemp * 60;
                    if (minute == 60) {
                        sb.append((hourTemp + 1) + "小时");
                    } else {
                        sb.append(hourTemp + "小时");
                        if (minute > 0) {
                            sb.append(minute + "分钟");
                        }
                    }
                } else {
                    sb.append(minute + "分钟");
                }
            }
        } else {
            if (minute - 1 > 0) {
                if (minute == 60) {
                    sb.append("1小时");
                } else {
                    sb.append(minute + "分钟");
                }
            } else if (mill - 1 > 0) {
                if (mill == 60) {
                    sb.append("1分钟");
                } else {
                    sb.append(mill + "秒");
                }
            } else {
                sb.append("刚刚");
            }
        }
        if (!sb.toString().contains("刚刚") && !sb.toString().contains(":")) {
            sb.append("前");
        }
        return "(" + sb.toString() + ")";
    }
    public static String getNearByTimeShow(long targetTime,boolean hasDistanceBeforeNow){
        Date targetDate = new Date(targetTime);
        if (isYesterday(targetTime)) {
            return  "昨天 " + getTimeSimpleShow(targetDate)+(hasDistanceBeforeNow?getDistanceBeforeNow(targetTime):"");
        } else if (isToday(targetDate)) {
            return  "今天 " + getTimeSimpleShow(targetDate)+(hasDistanceBeforeNow?getDistanceBeforeNow(targetTime):"");
        } else if(isThisYear(targetDate)){
            return getMonthDayHourMinuteDate(targetDate);
        }else {
            return getShowDateAllShow(targetDate);
        }
    }
    /**
     * 判断是否为昨天(效率比较高)
     */
    public static boolean isYesterday(long time) {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        pre.setTimeZone(TimeZone.getDefault());
        Calendar cal = Calendar.getInstance();
        Date date = new Date(time);
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getDefault());

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == -1) {
                return true;
            }
        }
        return false;
    }
    /**
     * 判断是否为昨天(效率比较高)
     */
    public static boolean isYesterday(Date date) {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        pre.setTimeZone(TimeZone.getDefault());
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getDefault());

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == -1) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否为今天(效率比较高)
     */
    public static boolean isToday(Date date) {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        pre.setTimeZone(TimeZone.getDefault());
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getDefault());
        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);
            if (diffDay == 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isThisYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone(TimeZone.getDefault());
        int year1 = calendar.get(Calendar.YEAR);

        calendar.setTime(new Date());
        int yearThis = calendar.get(Calendar.YEAR);
        return year1 == yearThis;
    }
    public static String getTimeShow(Date date) {
        String time = sfRecordShow.format(date);
        if (time.startsWith("0")) {
            return time.substring(1, time.length());
        }
        return time;
    }
}

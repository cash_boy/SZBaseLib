package com.cn.shuangzi.util;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class SaveNetPhotoUtil {

    private static Context contexts;
    private static String photoUrls;
    private static Bitmap bitmap;

    //自定义名字
    private static String photoNames;
    private static boolean isSuccess;

    /**
     * 保存图片，无须自定义名字
     *
     * @param context
     * @param photoUrl
     */
    public static void savePhoto(Context context, String photoUrl) {
        isSuccess = false;
        contexts = context;
        photoUrls = photoUrl;
        new Thread(saveFileRunnable).start();
    }

    /**
     * 定义图片名字保存到相册
     *
     * @param context
     * @param photoUrl
     * @param photoName 图片名字，定义格式 name.jpg/name.png/...
     */
    public static void savePhoto(Context context, String photoUrl, String photoName) {
        isSuccess = false;
        contexts = context;
        photoUrls = photoUrl;
        photoNames = photoName;
        new Thread(saveFileRunnable2).start();
    }

    private static Runnable saveFileRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (!TextUtils.isEmpty(photoUrls)) {
                    URL url = new URL(photoUrls);
                    InputStream inputStream = url.openStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                }
                saveImageToGallery(bitmap);
                isSuccess = true;
            } catch (IOException e) {
                isSuccess = false;
                e.printStackTrace();
            } catch (Exception e) {
                isSuccess = false;
                e.printStackTrace();
            }
            messageHandler.sendMessage(messageHandler.obtainMessage());
        }
    };

    private static Runnable saveFileRunnable2 = new Runnable() {
        @Override
        public void run() {
            try {
                if (!TextUtils.isEmpty(photoUrls)) {
                    URL url = new URL(photoUrls);
                    InputStream inputStream = url.openStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                }
                saveImageToGallery(bitmap, photoNames);
                isSuccess = true;
            } catch (IOException e) {
                isSuccess = false;
                e.printStackTrace();
            } catch (Exception e) {
                isSuccess = false;
                e.printStackTrace();
            }
            messageHandler.sendMessage(messageHandler.obtainMessage());
        }
    };

    /**
     * 保存成功和失败通知
     */
    @SuppressLint("HandlerLeak")
    private static Handler messageHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(isSuccess){
                SZToast.success("保存成功！");
            }else{
                SZToast.error("保存失败！");
            }
        }
    };

    /**
     * 保存图片
     *
     * @param bm
     * @throws IOException
     */
    public static boolean saveImageToGallery(Bitmap bm) throws IOException {
        return SZUtil.saveImageToGallery(contexts,bm);
    }

    /**
     * 保存图片
     *
     * @param bm
     * @param photoName 图片命名
     * @throws IOException
     */
    public static boolean saveImageToGallery(Bitmap bm, String photoName) throws IOException {
        return SZUtil.saveImageToGallery(contexts,bm,photoName);
    }
}


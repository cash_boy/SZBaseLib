package com.cn.shuangzi.fragment;


import android.annotation.SuppressLint;
import android.widget.LinearLayout;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseFragment;

/**
 * Created by CN on 2018-3-1.
 */

@SuppressLint("ValidFragment")
public class SZGuideFragment extends SZBaseFragment {
    private LinearLayout lltBg;
    private int imgBgRes;
    public SZGuideFragment(int imgBgRes){
        this.imgBgRes = imgBgRes;
    }
    @Override
    protected int onGetChildView() {
        return R.layout.fragment_guide;
    }

    @Override
    protected void onBindChildViews() {
        lltBg = findViewById(R.id.lltBg);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        lltBg.setBackgroundResource(imgBgRes);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }
}

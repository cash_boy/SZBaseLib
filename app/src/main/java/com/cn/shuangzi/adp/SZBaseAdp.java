package com.cn.shuangzi.adp;

import android.content.Context;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cn.shuangzi.R;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.util.SZGlide;
import com.cn.shuangzi.util.SZImageLoader;

import java.util.List;

/**
 * Created by CN.
 */

public abstract class SZBaseAdp<T extends Object> extends BaseQuickAdapter<T, BaseViewHolder> {
    private Context context;
    private SZImageLoader imageLoader;
    private BaseViewHolder baseViewHolder;

    public SZBaseAdp(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
        setEmptyView(false, 0, 0);
    }

    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
        this.context = context;
        if (context != null) {
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true, 0, R.mipmap.ic_empty);
    }

    public SZBaseAdp(int layoutResId, @Nullable List<T> data, View emptyView) {
        super(layoutResId, data);
        setEmptyView(emptyView);
    }

    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, View emptyView) {
        super(layoutResId, data);
        if (context != null) {
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(emptyView);
    }

    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, int txtResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if (context != null) {
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true, txtResEmpty, R.mipmap.ic_empty);
    }

    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, int txtResEmpty, int imgResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if (context != null) {
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true, txtResEmpty, imgResEmpty);
    }

    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, boolean hasEmpty, int txtResEmpty, int imgResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if (context != null) {
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(hasEmpty, txtResEmpty, imgResEmpty);
    }

    private void setEmptyView(boolean hasEmpty, int txtResEmpty, int imgResEmpty) {
        if (hasEmpty) {
            View emptyView = LayoutInflater.from(context).inflate(R.layout.item_empty, null);
            if (txtResEmpty != 0) {
                TextView txtEmpty = emptyView.findViewById(R.id.txtEmpty);
                txtEmpty.setText(txtResEmpty);
            }
            ImageView imgEmpty = emptyView.findViewById(R.id.imgEmpty);
            if (imgResEmpty > 0) {
                imgEmpty.setImageResource(imgResEmpty);
            } else if (imgResEmpty == -1) {
                imgEmpty.setVisibility(View.GONE);
            }
            setEmptyView(emptyView);
        }
    }

    public <T extends View> T getView(int viewId) {
        return getView(baseViewHolder,viewId);
    }

    public <T extends View> T getView(BaseViewHolder helper, int viewId) {
        return helper.getView(viewId);
    }

    public BaseViewHolder loadImg(Context context, int viewId, String url) {
        return loadImg(context,baseViewHolder,viewId,url);
    }

    public BaseViewHolder loadImg(Context context, int viewId, String url,int placeholder) {
        return loadImg(context,baseViewHolder,viewId,url,placeholder);
    }

    public BaseViewHolder loadImg(Context context,BaseViewHolder helper, int viewId, String url) {
        return loadImg(context,helper,viewId,url,0);
    }

    public BaseViewHolder loadImg(Context context,BaseViewHolder helper, int viewId, String url,int placeholder) {
        if (context != null) {
            SZGlide.load(context, url, (ImageView) getView(helper, viewId),placeholder);
        }
        return helper;
    }

    public BaseViewHolder loadImg(int viewId, String url) {
        return loadImg(baseViewHolder,viewId,url);
    }

    public BaseViewHolder loadImg(int viewId, String url,int placeholder) {
        return loadImg(baseViewHolder,viewId,url,placeholder);
    }

    public BaseViewHolder loadImg(BaseViewHolder helper, int viewId, String url) {
        return loadImg(helper,viewId,url,0);
    }

    public BaseViewHolder loadImg(BaseViewHolder helper, int viewId, String url,int placeholder) {
        if (context != null) {
            SZGlide.load(context, url, (ImageView) getView(helper, viewId),placeholder);
        }
        return helper;
    }

    public BaseViewHolder setTextSize(int viewId, int txtSizeDp) {
        return setTextSize(baseViewHolder,viewId,txtSizeDp);
    }

    public BaseViewHolder setTextSize(BaseViewHolder helper, int viewId, int txtSizeDp) {
        TextView textView = getView(helper,viewId);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, txtSizeDp);
        return helper;
    }


    public BaseViewHolder setVisibleWithGone(int viewId, boolean isVisible) {
        return setVisibleWithGone(baseViewHolder,viewId,isVisible);
    }
    public BaseViewHolder setVisibleWithGone(BaseViewHolder helper, int viewId, boolean isVisible) {
        if (isVisible) {
            helper.setVisible(viewId, true);
        } else {
            helper.setGone(viewId, true);
        }
        return helper;
    }

    public BaseViewHolder setVisibleWithInvisible(int viewId, boolean isVisible) {
        return setVisibleWithInvisible(baseViewHolder,viewId,isVisible);
    }

    public BaseViewHolder setVisibleWithInvisible(BaseViewHolder helper, int viewId, boolean isVisible) {
        helper.setVisible(viewId, isVisible);
        return helper;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, T item) {
        this.baseViewHolder = baseViewHolder;
        convertView(baseViewHolder,item);
    }

    public abstract void convertView(BaseViewHolder holder, T item);

    public BaseViewHolder getViewHolder() {
        return baseViewHolder;
    }

    public SZImageLoader getImageLoader() {
        return imageLoader;
    }

    public Context getAdpContext() {
        return context;
    }
}
